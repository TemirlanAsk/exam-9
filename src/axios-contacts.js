import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://homework-65-c4f7b.firebaseio.com/'
});

export default instance;