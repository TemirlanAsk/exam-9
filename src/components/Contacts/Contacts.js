import React from 'react';
import './Contacts.css';

const Contacts = props => {
    return (
        <div className='Contacts'>
            <div className="name">{props.name}</div>
            <div className='number'>{props.number}</div>
            <div className='email'>{props.email} Email:</div>
            <img className='pic' src={props.URL}/>
            <button onClick={props.delete} className='Delete'>Delete</button>
        </div>
    )
};

export default Contacts;