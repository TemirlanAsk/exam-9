import React, {Component} from 'react';
import './EditContact.css';
import {connect} from "react-redux";
import {editContact, showContacts} from "../../store/actions/contactBuilder";

class EditContact extends Component{
    state ={
        contacts: [],
        loading: false
    };
    componentDidMount() {
        const id = this.props.match.params.id;
        this.props.onShowContacts();
    }

    contactValueChanged = e => {
        e.persist();
        this.setState(prevState => {
            return {
                contact: {...prevState.contact, [e.target.name]: e.target.value}
            }
        });
    };
    render() {
        return(
            <form className="EditContact">
                <h1>Edit a Contact</h1>
                <input type="text" name="name" placeholder="Enter contact"
                       value={this.props.contact} onChange={this.contactValueChanged}/>
                <input name="number" placeholder="Enter your number"
                       value={this.props.number} onChange={this.contactValueChanged}
                />
                <input name="email" placeholder="Enter your email"
                       value={this.props.email} onChange={this.contactValueChanged}
                />
                <input name="url" placeholder="Enter image url"
                       value={this.props.url} onChange={this.contactValueChanged}
                />
                <button onClick={this.onContactEdited}>Save</button>
            </form>
        )
    }
}
const mapStateToProps = state => {
    return {
        contact: state.contacts.name,
        number: state.contacts.number,
        email: state.contacts.email,
        url: state.contacts.url
    }
};
const mapDispatchToProps = dispatch => {
    return {
        onContactEdited: (name) => dispatch(editContact(name)),
        onShowContacts: () => dispatch(showContacts())
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(EditContact);