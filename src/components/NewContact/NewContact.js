import React, {Component} from 'react';
import {connect} from "react-redux";
import './NewContact.css';
import {addNewContact} from "../../store/actions/contactBuilder";


class  NewContact extends Component {
    state =  {
        contact: [],
        loading: false
    };

    contactCreateHandler = e => {
        this.props.onContactAdded();
        this.props.history.replace('/');
    };

    contactValueChanged = e => {
        e.persist();
        this.setState(prevState => {
            return {
                contact: {...prevState.contact, [e.target.name]: e.target.value}
            }
        });
    };

    render(){
        return(
            <form className="NewContact">
                <h1>New Contact</h1>
                <input type="text" name="name" placeholder="Enter contact"
                       value={this.props.contact} onChange={this.contactValueChanged}/>
                <input name="number" placeholder="Enter your number"
                       value={this.props.number} onChange={this.contactValueChanged}
                />
                <input name="email" placeholder="Enter your email"
                       value={this.props.email} onChange={this.contactValueChanged}
                />
                <input name="url" placeholder="Enter image url"
                       value={this.props.url} onChange={this.contactValueChanged}
                />
                <button onClick={this.contactCreateHandler}>Save</button>
            </form>
        )
    }
}
const mapStateToProps = state => {
    return {
        contact: state.contacts.name,
        number: state.contacts.number,
        email: state.contacts.email,
        url: state.contacts.url
    }
};
const mapDispatchToProps = dispatch => {
    return {
        onContactAdded: (name) => dispatch(addNewContact(name)),
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(NewContact)
