import React, {Component, Fragment} from 'react';
import './App.css';
import {NavLink, Route, Switch} from "react-router-dom";
import ContactList from "./containers/ContactList/ContactList";
import NewContact from "./components/NewContact/NewContact";
import EditContact from "./components/EditContact/EditContact";

class App extends Component {
  render() {
    return (
        <Fragment>
            <Switch>
                <Route path="/" exact component={ContactList}/>
                <Route path="/add" component={NewContact}/>
                <Route path="/edit/:id" component={EditContact}/>
                <Route render={() =>  <h1>Not Found</h1>}/>
            </Switch>
        </Fragment>
    );
  }
}

export default App;
