import React, {Fragment} from 'react';
import './ContactList.css';
import {connect} from 'react-redux';
import {NavLink} from "react-router-dom";
import Contacts from "../../components/Contacts/Contacts";
import {deleteContact, editContact, showContacts} from "../../store/actions/contactBuilder";

class ContactList extends React.Component {

    componentDidMount() {
        this.props.onShowContacts();
    }

    contactEditHandler = (id) => {
        this.props.onContactEdited();
        this.props.history.replace('/edit');
    };

    render() {
        return(
            <Fragment>
                <h1>Contacts</h1>
                <NavLink className='AddContact' to={'/add'}>Add new Contact</NavLink>
                <div>
                    {Object.keys(this.props.contacts).map(contactId => (
                        <Contacts
                            key={contactId}
                            name={this.props.contacts[contactId].name}
                            number={this.props.contacts[contactId].number}
                            email={this.props.contacts[contactId].email}
                            pic={this.props.contacts[contactId].url}
                            delete={() => this.props.onContactDeleted(contactId)}
                            edit={() => this.contactEditHandler(contactId)}
                        />
                    ))}
                </div>
            </Fragment>
        )
    }
}
const mapStateToProps = state => {
    return {
        contacts: state.contacts,
    }
};
const mapDispatchToProps = dispatch => {
    return {
        onContactDeleted: (contactId) => dispatch(deleteContact(contactId)),
        onContactEdited: (name) => dispatch(editContact(name)),
        onShowContacts: () => dispatch(showContacts())
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ContactList);