import React, {Component, Fragment} from 'react';
import Modal from "../../../components/UI/Modal/Modal";


const withContactHandler = (WrappedComponent, axios) => {
        return class extends Component {
            constructor(props){
                super(props);
                this.state = {
                    modal: false,
                    showModal: false

                };
                componentDidMount(){
                    this.props.onShowContacts();
                }
                showModal = (e, id) => {
                  e.preventDefault();
                  this.props.history.replace("/");
                  this.setState({showModal: true,})
                }

            }

            render(){
                return(
                    <Fragment>
                        <Modal>

                        </Modal>
                        <WrappedComponent {...this.props}/>
                    </Fragment>
                )
            }
        }
};
export default withContactHandler;