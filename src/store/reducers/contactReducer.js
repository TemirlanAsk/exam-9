import * as actionTypes from "../actions/actionTypes";

const initialState = {
    contacts: {}
};
const reducer = (state = initialState, action) => {

    switch (action.type) {
        case actionTypes.CONTACT_SUCCESS:
            return{contacts:action.info};

        case actionTypes.DELETE_CONTACT:

            return {};
        case actionTypes.ADD_NEW_CONTACT:

            return {};
        case actionTypes.EDIT_CONTACT:

            return {};
        default:
            return state;

    }
};
export default reducer;